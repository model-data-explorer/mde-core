.. _api:

API Reference
=============

.. toctree::
    :maxdepth: 1

    api/mde_core.app_settings
    api/mde_core.urls
    api/mde_core.models
    api/mde_core.views


.. toctree::
    :hidden:

    api/modules
