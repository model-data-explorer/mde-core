.. _installation:

Installation
============

To install the `mde-core` package for your Django project, you need
to follow two steps:

1. :ref:`Install the package <install-package>`
2. :ref:`Add the app to your Django project <install-django-app>`

.. _install-package:

Installation from PyPi
----------------------
The recommended way to install this package is via pip and PyPi via::

    pip install mde-core

Or install it directly from `the source code repository on Gitlab`_ via::

    pip install git+https://gitlab.hzdr.de/model-data-explorer/mde-core.git

The latter should however only be done if you want to access the development
versions.

.. _the source code repository on Gitlab: https://gitlab.hzdr.de/model-data-explorer/mde-core


.. _install-django-app:

Install the Django App for your project
---------------------------------------
To use the `mde-core` package in your Django project,
you need to add the app to your `INSTALLED_APPS`, configure your `urls.py`, run the
migration, add a login button in your templates. Here are the step-by-step
instructions:

1. Add the `mde_core` app to your `INSTALLED_APPS`
2. in your projects urlconf (see :setting:`ROOT_URLCONF`), add include
   :mod:`mde_core.urls` via::

       from django.urls import include, path

       urlpatterns += [
           path("mde-core/", include("mde_core.urls")),
        ]
3. Run ``python manage.py migrate`` to add models to your database
4. Configure the app to your needs (see :ref:`configuration`).

That's it! For further adaption to you Django project, please head over to the
:ref:`configuration`. You can also have a look into the ``testproject``
in the `source code repository`_ for a possible implementation.

.. _source code repository: https://gitlab.hzdr.de/hcdc/django/mde-core


.. _install-develop:

Installation for development
----------------------------
Please head over to our :ref:`contributing guide <contributing>` for
installation instruction for development.
