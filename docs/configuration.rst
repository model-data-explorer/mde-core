.. _configuration:

Configuration options
=====================

Configuration settings
----------------------

The following settings have an effect on the app.

.. automodulesumm:: mde_core.app_settings
    :autosummary-no-titles:
