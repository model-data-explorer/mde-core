"""Admin interfaces
----------------

This module defines the mde-core
Admin interfaces.
"""

# Disclaimer
# ----------
#
# Copyright (C) 2022 Helmholtz-Zentrum Hereon
#
# This file is part of mde-core and is released under the
# EUPL-1.2 license.
# See LICENSE in the root of the repository for full licensing details.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 or later
# as published by the European Commission.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# EUPL-1.2 license for more details.
#
# You should have received a copy of the EUPL-1.2 license along with this
# program. If not, see https://www.eupl.eu/.
from __future__ import annotations

from typing import Sequence

from django.contrib import admin  # noqa: F401

from mde_core import models  # noqa: F401


@admin.register(models.Dataset)
class DatasetAdmin(admin.ModelAdmin):
    """An admin interface for the dataset"""

    filter_horizontal: Sequence[str] = ["tags"]

    pass


@admin.register(models.Tag)
class TagAdmin(admin.ModelAdmin):
    """An admin interface for tags."""

    pass


@admin.register(models.DataGroup)
class DataGroupAdmin(admin.ModelAdmin):
    """An admin for a datagroup."""

    list_display = [
        "__str__",
        "is_root_parent_tree",
        "is_root_member_tree",
        "is_root_list_tree",
    ]

    @admin.display(boolean=True)
    def is_root_parent_tree(self, obj: models.DataGroup):
        node = obj.datagroupnode_set.get(
            tree_type=models.DataGroupNode.TreeType.parent_tree
        )
        return node.is_root()

    @admin.display(boolean=True)
    def is_root_member_tree(self, obj: models.DataGroup):
        node = obj.datagroupnode_set.get(
            tree_type=models.DataGroupNode.TreeType.member_tree
        )
        return node.is_root()

    @admin.display(boolean=True)
    def is_root_list_tree(self, obj: models.DataGroup):
        node = obj.datagroupnode_set.get(
            tree_type=models.DataGroupNode.TreeType.list_tree
        )
        return node.is_root()


@admin.register(models.DataGroupEdge)
class DataGroupEdgeAdmin(admin.ModelAdmin):

    """An admin for a DataGroupEdge."""

    pass


@admin.register(models.DataGroupRelation)
class DataGroupRelation(admin.ModelAdmin):

    """An admin for a DataGroupRelation"""

    list_display = [
        "__str__",
        "parent_approved",
        "child_approved",
        "tree_type",
    ]

    list_filter = ["parent_approved", "child_approved", "tree_type"]

    search_fields: Sequence[str] = [
        "parent_group__name__icontains",
        "child_group__name__icontains",
    ]


@admin.register(models.DatasetDataGroupRelation)
class DatasetDataGroupRelationAdmin(admin.ModelAdmin):
    """An admin for a DatasetDataGroupRelation."""

    list_display = [
        "__str__",
        "dataset_approved",
        "related_party_approved",
        "role",
    ]

    list_filter = ["dataset_approved", "related_party_approved", "role"]

    search_fields: Sequence[str] = [
        "dataset__name__icontains",
        "related_party__name__icontains",
    ]
