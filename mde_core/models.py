"""Models
------

Models for the mde-core app.
"""

# Disclaimer
# ----------
#
# Copyright (C) 2022 Helmholtz-Zentrum Hereon
#
# This file is part of mde-core and is released under the
# EUPL-1.2 license.
# See LICENSE in the root of the repository for full licensing details.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 or later
# as published by the European Commission.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# EUPL-1.2 license for more details.
#
# You should have received a copy of the EUPL-1.2 license along with this
# program. If not, see https://www.eupl.eu/.


from __future__ import annotations

from uuid import uuid4

from django.contrib.auth.models import Group, User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django_postgresql_dag.models import edge_factory, node_factory

from mde_core import app_settings  # noqa: F401


class DataGroupEdge(edge_factory("mde_core.DataGroupNode", concrete=False)):  # type: ignore
    def __str__(self):
        return (
            f"{self.parent.tree_type}: {self.parent.datagroup} "
            "→ {self.child.datagroup}"
        )


class DataGroupNode(node_factory(DataGroupEdge)):  # type: ignore
    """A node to display relations between data groups."""

    class Meta:

        unique_together = ("tree_type", "datagroup")

    class TreeType(models.TextChoices):
        """Type of the tree for a datagroup."""

        parent_tree = "PARENT", "Parent-Child graph"
        member_tree = "MEMBER", "Member inheritance graph"
        list_tree = "LIST", "Dataset listing graph"

    tree_type = models.CharField(max_length=10, choices=TreeType.choices)
    datagroup = models.ForeignKey("DataGroup", on_delete=models.CASCADE)

    def __str__(self) -> str:
        return f"{self.datagroup} in {self.tree_type} tree"


class DataGroupUserGroup(Group):
    """A group with a certain role in a :class:`DataGroup`"""

    class Roles(models.TextChoices):
        owner = "OWNER", "owner priviliges"
        data_manager = "DATAMANAGER", "data manager privileges"
        data_editor = "DATAEDITOR", "data editor privileges"
        editor = "EDITOR", "editor privileges"
        member = "MEMBER", "view permissions"

    datagroup = models.ForeignKey("DataGroup", on_delete=models.CASCADE)
    role = models.CharField(max_length=20, choices=Roles.choices)


class DataGroupRelation(models.Model):
    """An relation between two datagroups that awaits approval."""

    class Meta:

        unique_together = ("child_group", "parent_group", "tree_type")

    child_group = models.ForeignKey(
        "DataGroup", on_delete=models.CASCADE, related_name="%(class)s_child"
    )
    parent_group = models.ForeignKey(
        "DataGroup", on_delete=models.CASCADE, related_name="%(class)s_parent"
    )

    child_approved = models.BooleanField(default=False)
    parent_approved = models.BooleanField(default=False)

    child_approved_by = models.ForeignKey(
        User,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="%(class)s_child_approval",
    )
    parent_approved_by = models.ForeignKey(
        User,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="%(class)s_parent_approval",
    )

    tree_type = models.CharField(
        max_length=10, choices=DataGroupNode.TreeType.choices
    )

    def __str__(self) -> str:
        return f"Relation between {self.parent_group} and {self.child_group}"


class DataGroup(models.Model):
    """A DataGroup in the Model Data Explorer"""

    class Meta:

        app_label = "mde_core"

    datagroupnode_set: models.manager.RelatedManager[DataGroupNode]

    uuid = models.UUIDField(primary_key=True, default=uuid4)
    name = models.CharField(max_length=100)
    tags = models.ManyToManyField("Tag", blank=True)

    def __str__(self):
        return self.name


class DataGroupUserRelation(models.Model):
    """A direct relation between a user and a data group."""

    class Meta:
        unique_together = ("datagroup", "user", "role")

    datagroup = models.ForeignKey(DataGroup, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    role = models.CharField(
        max_length=20, choices=DataGroupUserGroup.Roles.choices
    )

    datagroup_approved_by = models.ForeignKey(
        User,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="datagroupuserrelation_datagroup_approval",
    )
    user_approved_by = models.ForeignKey(
        User,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="datagroupuserrelation_user_approval",
    )


class Dataset(models.Model):
    """A dataset in the model data explorer."""

    uuid = models.UUIDField(primary_key=True, default=uuid4)
    name = models.CharField(max_length=100)
    tags = models.ManyToManyField("Tag", blank=True)

    @property
    def list_datagroups(self) -> models.QuerySet[DataGroup]:
        """Get the datagroups with list permissions on the dataset."""
        # get pks of directly linked datagroups
        datagroup_pks = (
            self.datasetdatagrouprelation.get_approved().values_list(
                "related_party__pk", flat=True
            )
        )

        # get the corresponding nodes in the list graph
        nodes = DataGroupNode.objects.filter(
            datagroup__pk__in=datagroup_pks,
            tree_type=DataGroupNode.TreeType.list_tree,
        )

        # get the pks of the parents in the list graph
        all_datagroup_pks = set()
        for node in nodes:
            all_datagroup_pks.update(
                node.ancestors_and_self().values_list(
                    "datagroup__pk", flat=True
                )
            )

        # filter for all datagroups
        return DataGroup.objects.filter(pk__in=all_datagroup_pks)

    def __str__(self) -> str:
        return self.name


class DatasetRelationBaseQueryset(models.QuerySet):
    def get_approved(self, **kwargs):
        return self.filter(
            related_party_approved=True, dataset_approved=True, **kwargs
        )


class DatasetRelationBaseManager(
    models.Manager.from_queryset(DatasetRelationBaseQueryset)  # type: ignore
):
    pass


class DatasetRelationBase(models.Model):
    """Abstract base for a relation between dataset and user or group."""

    objects = DatasetRelationBaseManager()

    class Meta:

        unique_together = ("dataset", "related_party", "role")
        abstract = True

    class Roles(models.TextChoices):
        owner = "OWNER", "owner priviliges"
        data_manager = "DATAMANAGER", "data manager privileges"
        data_editor = "DATAEDITOR", "data editor privileges"
        editor = "EDITOR", "editor privileges"
        member = "MEMBER", "view permissions"

    dataset = models.ForeignKey(
        "Dataset", on_delete=models.CASCADE, related_name="%(class)s"
    )
    role = models.CharField(max_length=20, choices=Roles.choices)

    dataset_approved = models.BooleanField(default=False)
    related_party_approved = models.BooleanField(default=False)

    dataset_approved_by = models.ForeignKey(
        User,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="%(class)s_dataset_approval",
    )
    related_party_approved_by = models.ForeignKey(
        User,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="%(class)s_related_party_approval",
    )

    def __str__(self) -> str:
        return f"{self.role} of {self.related_party} for {self.dataset}"  # type: ignore


class DatasetDataGroupRelation(DatasetRelationBase):
    """A permission for a relation."""

    related_party = models.ForeignKey(DataGroup, on_delete=models.CASCADE)


class DatasetUserRelation(DatasetRelationBase):
    """A permission for a relation."""

    related_party = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="%(class)s_related_party"
    )


class TagEdge(edge_factory("mde_core.Tag", concrete=False)):  # type: ignore
    """An edge for a DataGroup or Dataset Tag."""

    def __str__(self):
        return (
            f"{self.parent.tree_type}: {self.parent.datagroup} "
            "→ {self.child.datagroup}"
        )


class Tag(node_factory(TagEdge)):  # type: ignore
    """A tag for a DataGroup or Dataset."""

    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class TagAlias(models.Model):
    """An alternative name for a Tag."""

    name = models.CharField(max_length=100)

    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)


@receiver(post_save, sender=DataGroup)
def create_datagroup_nodes(instance: DataGroup, created: bool, **kwargs):
    if created:
        instance.datagroupnode_set.create(
            tree_type=DataGroupNode.TreeType.parent_tree
        )
        instance.datagroupnode_set.create(
            tree_type=DataGroupNode.TreeType.member_tree
        )
        instance.datagroupnode_set.create(
            tree_type=DataGroupNode.TreeType.list_tree
        )


@receiver(post_save, sender=DataGroupRelation)
def update_datagroup_tree(instance: DataGroupRelation, **kwargs):
    parent_node = DataGroupNode.objects.get_or_create(
        tree_type=instance.tree_type, datagroup=instance.parent_group
    )[0]
    child_node = DataGroupNode.objects.get_or_create(
        tree_type=instance.tree_type, datagroup=instance.child_group
    )[0]
    if instance.parent_approved and instance.child_approved:
        parent_node.add_child(child_node)
    else:
        try:
            edge = DataGroupEdge.objects.get(
                parent=parent_node, child=child_node
            )
        except DataGroupEdge.DoesNotExist:
            pass
        else:
            edge.delete()
