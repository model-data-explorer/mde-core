"""URL config
----------

URL patterns of the mde-core to be included via::

    from django.urls import include, path

    urlpatters = [
        path(
            "mde-core",
            include("mde_core.urls"),
        ),
    ]
"""

# Disclaimer
# ----------
#
# Copyright (C) 2022 Helmholtz-Zentrum Hereon
#
# This file is part of mde-core and is released under the
# EUPL-1.2 license.
# See LICENSE in the root of the repository for full licensing details.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 or later
# as published by the European Commission.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# EUPL-1.2 license for more details.
#
# You should have received a copy of the EUPL-1.2 license along with this
# program. If not, see https://www.eupl.eu/.
from __future__ import annotations

from typing import Any

from django.urls import path  # noqa: F401

from mde_core import views  # noqa: F401

#: App name for the mde-core to be used in calls to
#: :func:`django.urls.reverse`
app_name = "mde_core"

#: urlpattern for the Helmholtz AAI
urlpatterns: list[Any] = []
